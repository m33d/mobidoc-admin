import { Ang2bPage } from './app.po';

describe('ang2b App', function() {
  let page: Ang2bPage;

  beforeEach(() => {
    page = new Ang2bPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
